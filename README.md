# Taktische Zeichen Vorlagen Generator

Erstelle Druckvorlagen für Taktische Zeichen in einem einfachen Web UI.

[![Jetzt Druckvorlage erstellen](https://img.shields.io/badge/-Jetzt%20Druckvorlage%20erstellen-success)](https://taktische-zeichen.org/)

Basierend auf den [Taktischen Zeichen von Jonas Köritz](https://github.com/jonas-koeritz/Taktische-Zeichen).

## Update `symbols.json`

```shell script
(cd public; find svg/ -type f -name '*.svg') | sort | \
  while read P; \
    do B=$(dirname "$P"); G=$(basename "$B" | tr "_" " "); F=$(basename "$P" ".svg" | tr "_" " "); \
    jq -r --arg P "$P" --arg G "$G" --arg F "$F" '. | .path=$P | .group=$G | .name=$F' <<< '{}'; \
    done | \
    jq -s 'group_by(.group)[] | {(.[0].group): [.[]]}' | \
    jq -s add | \
    tee src/symbols.json
```

## License

Der *Taktische Zeichen Vorlagen Generator* steht unter der [Apache License 2.0](LICENSE).

Die *Taktischen Zeichen von Jonas Köritz* stehen unter der [Apache License 2.0](public/svg/LICENSE.txt); [Copyright Hinweis](public/svg/COPYRIGHT.md).
